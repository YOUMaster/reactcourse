console.log(React);
console.log(ReactDOM);

// ReactDOM.render(
// 	React.createElement('h1', null, 'Привет, Мир!'),
// 	document.getElementById('root')
// );

//альтернатива кода выше
ReactDOM.render(
	<h1>Hello, world!</h1>,
	document.getElementById('root')
);

//создали компонент
var App = React.createClass({
	render: function() {
		return (
			<div className="app">
				Всем привет, я компонент App!
			</div>
		);
	}
});

ReactDOM.render(
	<App />,
	document.getElementById('root')
);


//использование вложенных компонентов
var my_news = [
{
	author: 'Саша Печкин',
	text: 'В четчерг, четвертого числа...',
	bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
},
{
	author: 'Просто Вася',
	text: 'Считаю, что $ должен стоить 35 рублей!',
	bigText: 'А евро 42!'
},
{
	author: 'Гость',
	text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
	bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
	}
];

var News = React.createClass({
	//валидация
	propTypes: {
		data: React.PropTypes.array.isRequired
	},

	getInitialState: function() {
		return {
			counter: 0
		}
	},
	onTotalNewsClick: function() {
		this.setState({counter: ++this.state.counter });
	},

	render: function() {
		var data = this.props.data;
		var newsTemplate;
		//шаблон
		if (data.length > 0) {
			var newsTemplate = data.map(function(item, index) {
				return (
					<div key={index}>
						<Article data={item} />
					</div>
				)
			})
		} else {
			newsTemplate = <p>К сожалению новостей нет</p>
		}
		return (
			//вывод шаблона
			<div className="news">
				{newsTemplate}
				<strong 
					onClick={this.onTotalNewsClick} 
					className={'news__count' + (data.length > 0 ? '':'none') }>
					Всего новостей: {data.length}
				</strong>
			</div>
		);
	}
});

var Article = React.createClass({
	//валидация
	propTypes: {
		data: React.PropTypes.shape({
			author: React.PropTypes.string.isRequired,
			text: React.PropTypes.string.isRequired,
			bigText: React.PropTypes.string.isRequired
		})
	},

	//начальное состояние (в терминологии react.js - initial state)
	getInitialState: function() {
		return {
			visible: false
		};
	},

	//обработчик onClick
	readmoreClick: function(e) {
		e.preventDefault();
		this.setState({visible: true});
	},

	render: function() {
		var author = this.props.data.author,
				text = this.props.data.text,
				bigText = this.props.data.bigText,
				visible = this.state.visible; // считываем значение переменной из состояния компонента

		console.log('render',this); //добавили console.log

		return (
			<div className="article">
				<p className="news__author">{author}:</p>
				<p className="news__text">{text}</p>
				{/* для ссылки readmore: не показывай ссылку, если visible === true */}
				<a href="#" 
					onClick={this.readmoreClick} 
					className={'news__readmore ' + (visible ? 'none': '')}>
					Подробнее
				</a>
				{/* для большо текста: не показывай текст, если visible === false */}
				<p className={'news__big-text ' + (visible ? '': 'none')}>{bigText}</p>
			</div>
		)
	}
});

var App = React.createClass({
	render: function() {
		return (
			<div className="app">
				<h3>Новости</h3>
				<News data={my_news} /> {/*добавили свойство data */}
			</div>
		);
	}
});

ReactDOM.render(
	<App />,
	document.getElementById('root')
);